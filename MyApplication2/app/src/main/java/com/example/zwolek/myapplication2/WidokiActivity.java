package com.example.zwolek.myapplication2;

import android.app.ActionBar;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class WidokiActivity extends ActionBarActivity {

    private EditText mEditText;
   // private TextView mTextField;
    private Button mButton;
    private LinearLayout mLinearLayout;
    private LinearLayout.LayoutParams mKontrolkaParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    private static final String TAG = WidokiActivity.class.getName();

    // (ustawia widok odpalanego w aplikacji )
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Startuje widok odpowiednio wybrany z folderu layout
        //// Widok stworzony w activity_xxxx.xml
        setContentView(R.layout.activity_widoki);
        //// Widok stworzony w kodzie ale korzysta activity_xxxx.xml
        // setContentView(R.layout.activity_widok2);

        // findViewById musi byc koniecznie po setContenView
        mEditText = findView(R.id.pole_tekstowe);
//        mTextField = (TextView) findViewById(R.id.label);
        mButton = (Button) findViewById(R.id.przycisk);
        mLinearLayout = (LinearLayout) findViewById(R.id.teksty_kontener);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikPrzycisk(v);

            }
        });
        //    widokiZKodu();

    }


    public void widokiZKodu() {
        mLinearLayout = new LinearLayout(this);
        mLinearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        mLinearLayout.setLayoutParams(mParams);

        // wspolne atrybuty


        mEditText = new EditText(this);
        mEditText.setLayoutParams(mKontrolkaParams);
        mEditText.setSingleLine(true);
        mEditText.setHint("Podaj tekst");

//        mTextField = new TextView(this);
//        mTextField.setLayoutParams(mKontrolkaParams);

        LinearLayout.LayoutParams mGuzikParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mButton = new Button(this);
        mButton.setLayoutParams(mGuzikParams);
        mButton.setText("Gotowe");
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikPrzycisk(v);

            }
        });
        mLinearLayout.addView(mEditText);
       // mLinearLayout.addView(mTextField);
        mLinearLayout.addView(mButton);
        //// Wstawienie do kontenera obecnego w .xml
        //((RelativeLayout)findViewById(R.id.kontener)).addView(mLinearLayout);
        //// Opcaj bez plików actvity_xxxx.xml
        setContentView(mLinearLayout);
    }


    public void klikPrzycisk(View view) {
        //// pobieranie wartosci z plikow resources
        String restString = getResources().getString(R.string.imie_hint);

        String text = mEditText.getText().toString();
        Log.d(TAG, text != null ? "Wpisales za krotki test " + text.length() : "Null");
        if (text == null || text.length() < 3) {
            Toast.makeText(this, "Wpisales za krotki test", Toast.LENGTH_LONG).show();
        }
      //  mTextField.setText(""); //czyscimy tekst
        //mTextField.setText(text);
        TextView nNowePole = new TextView(this);
        nNowePole.setLayoutParams(mKontrolkaParams);
        nNowePole.setText(text);
        mLinearLayout.addView(nNowePole);


    }

    public <T extends View> T findView(int identyfikator) {

        return (T) findViewById(identyfikator);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }

    protected void wyczyscLinie() {
        List<View> mKolekcjaDoUsuniecia = new ArrayList<View>();
        for (int i = 0; i < mLinearLayout.getChildCount(); i++) {
            View mChild = mLinearLayout.getChildAt(i);
            mKolekcjaDoUsuniecia.add(mChild);
        }

        for (View view : mKolekcjaDoUsuniecia) {
            View mChild = view;
            // if(mChild.getClass().equals(TextView.class)){
            if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button)) {
                mLinearLayout.removeView(view);
            }
        }
    }

    private void wyczyscOstatni() {
        View mChild = mLinearLayout.getChildAt(mLinearLayout.getChildCount() - 1);
        if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button)) {
            mLinearLayout.removeView(mChild);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.usun_wszystko) {
            wyczyscLinie();
            return true;
        }

        if (id == R.id.usun_ostatni) {
            wyczyscOstatni();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
